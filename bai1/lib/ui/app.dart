import 'package:flutter/material.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Form nhập liệu",
      home: Scaffold(
        appBar: AppBar(title: Text('Form nhập thông tin')),
        body: FormScreen(),
      )
    );
  }

}

class FormScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<StatefulWidget>{
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20),),
            fieldFirstName(),
            Container(margin: EdgeInsets.only(top: 20),),
            fieldName(),
            Container(margin: EdgeInsets.only(top: 20),),
            fieldBrithYear(),
            Container(margin: EdgeInsets.only(top: 20),),
            fieldAddress(),
            Container(margin: EdgeInsets.only(top: 20),),
            submitButton()
          ],
        ),
      ),
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Địa chỉ Email'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Please enter valid email!';
        }
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Họ và chữ lót'
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please enter a name';
        }
        return null;
      },
    );
  }

  Widget fieldName() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Tên'
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please enter a name';
        }
        return null;
      },
    );
  }

  Widget fieldBrithYear() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.favorite),
          labelText: 'Năm sinh'
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please enter a year';
        }
        return null;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.home),
          labelText: 'Địa chỉ'
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Please enter address';
        }
        return null;
      },
    );
  }

  Widget submitButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            print('Submit successfully!');
          }
        },
        child: Text('Gửi'));
  }
}